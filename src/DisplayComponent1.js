import React from 'react';
import PropTypes from 'prop-types';

class DisplayComponent1 extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            isExpanded: false
        }
    }

    handleToggle(e){
        e.preventDefault();
        this.setState({
            isExpanded: !this.state.isExpanded,
        })
    }

    render(){
        const {title} = this.props;
        const {isExpanded, height} = this.state;
        let make = this.props.make; let sold = this.props.sold;
        return (
            <tr className={`panel ${isExpanded ? 'is-expanded' : ''}`} onClick={(e) => this.handleToggle(e)}>
                    <td><button onClick={() => {this.props.handler(make);}} type="button" className="btn btn-blue-grey btn-sm m-0">{make}</button> &nbsp; <i className="fa fa-automobile fa-lg blue-grey-text" aria-hidden="true" data-toggle="modal" data-target="#fullHeightModalRight"></i></td>
                    <td>{sold}</td>
                    <td>This is some filler text. Lorem Ipsum.</td>
            </tr>
        )
    }

}

DisplayComponent1.propTypes = {
    title: PropTypes.string,
};

export default DisplayComponent1;