import React from 'react';
import PropTypes from 'prop-types';

class DisplayComponent2 extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            isExpanded: false
        }
    }

    handleToggle(e){
        e.preventDefault();
        this.setState({
            isExpanded: !this.state.isExpanded,
        })
    }

    render(){
        const {title} = this.props;
        const {isExpanded, height} = this.state;
        let model = this.props.model; let NJ = this.props.NJ; let PA = this.props.PA; let CT = this.props.CT;
        return (
            <tr className={`panel ${isExpanded ? 'is-expanded' : ''}`} onClick={(e) => this.handleToggle(e)}>
            {/* <div> */}
                    <td>{model} &nbsp; <i class="fa fa-database fa-lg grey-text" aria-hidden="true" data-toggle="modal" data-target="#fullHeightModalBottom"></i></td>
                    <td>{NJ}</td>
                    <td>{PA}</td>
                    <td>{CT}</td>
            </tr>
        )
    }

}


    DisplayComponent2.propTypes = {
        title: PropTypes.string,
    };
    
    export default DisplayComponent2;