import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import DisplayComponent1 from './DisplayComponent1';
import DisplayComponent2 from './DisplayComponent2';

class App extends Component {

  constructor(props){
    super(props);
    this.handler = this.handler.bind(this)
    this.state = {
        isLoadingDisplay1: true,
        display1: "hidden",
        isLoadingDisplay2: true,
        display2: "hidden",
    }
  }

  handler(input) {
    console.log(input);
    let make = input;
    //e.preventDefault()
    this.setState({
      selectedMake: "Detailed Sale Information for " + input,
      isLoadingDisplay2: false,
      display2: "visible",
    })
    var endPoint = "";
    if(make == 'BMW')
        {
          endPoint = "//api.jsonbin.io/b/5ba6bbbd20f16433785dcba8/2"; 
        }
        else if(make == 'AUDI')
        {
          endPoint = "//api.jsonbin.io/b/5ba6bbdd74ca4633aae17d12/2";
        }
        else if(make == 'Mercedes')
        {
          endPoint = "//api.jsonbin.io/b/5ba6bbfc0fbf2833e22a1f28/3";
        }

        else if(make == 'Toyota')
        {
          endPoint = "//api.jsonbin.io/b/5ba7e1ef9353c37b743319a8/2";
        }
        else if(make == 'Honda')
        {
          endPoint = "//api.jsonbin.io/b/5ba7e20d8713b17b52acf586/2";
        }
        else if(make == 'Nissan')
        {
          endPoint = "//api.jsonbin.io/b/5ba7e22f8713b17b52acf59c/1";
        }
        
        // this.setState({
        // })
    
        fetch(endPoint)
         .then(response => response.json())
         .then(parsedJSON => parsedJSON.map(cars2 => (
            {
                model: `${cars2.model}`,
                NJ: `${cars2.NJ}`,
                PA: `${cars2.PA}`,
                CT: `${cars2.CT}`
            }
        )))
        .then(cars2 => this.setState({
            cars2,
            isLoading: false
          }))
        .then(console.log('JSON loaded'))
        .catch(error => console.log('parsing failed', error))
  }

  componentDidMount(){
         this.fetchData("German");
  }

  fetchData(importType){

    this.setState({
        isLoadingDisplay1: true,
        display1: "hidden",
        isLoadingDisplay2: true,
        display2: "hidden",
        importType: importType,
        cars: [],
        cars2: []
    })

    var endPoint = "//api.jsonbin.io/b/5ba6baf020f16433785dcb6d/3";
    if(importType == 'German')
    {
      endPoint = "//api.jsonbin.io/b/5ba6baf020f16433785dcb6d/3"; 
    }
    else if(importType == 'Japanese')
    {
      endPoint = "//api.jsonbin.io/b/5ba7e11ea97c597b3c56df47/1"; 
    }

    fetch(endPoint)
    .then(response => response.json())
    .then(parsedJSON => parsedJSON.map(cars => (
        {
            make: `${cars.make}`,
            sold: `${cars.sold}`
        }
    )))
    .then(cars => this.setState({
        cars,
        isLoadingDisplay1: false,
        display1: "visible",
      }))
    .then(console.log('JSON loaded'))
    .catch(error => console.log('parsing failed', error))
    
  }
  
  render() {
    const {isLoadingDisplay1, isLoadingDisplay2, cars, cars2} = this.state;
    return (
      <div className="App">
        <div>
                <header>
                    <div class="btn-group">
                    <button className="btn btn-sm btn-danger" onClick={(e) => {
                        this.fetchData("German");    
                    }}>Hard Reload App</button>
                    &nbsp;&nbsp;&nbsp;
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Change Imports</button>

                    <div class="dropdown-menu">
                        <a class="dropdown-item" onClick={(e) => {this.fetchData("German");}} href="#">German Imports</a>
                        <a class="dropdown-item" onClick={(e) => {this.fetchData("Japanese");}} href="#">Japanese Imports</a>
                    </div>
                    </div>
                      <p>clicking <i class="fa fa-automobile fa-lg blue-grey-text" aria-hidden="true" data-toggle="modal" data-target="#fullHeightModalRight"></i> and <i class="fa fa-database fa-lg grey-text" aria-hidden="true" data-toggle="modal" data-target="#fullHeightModalBottom"></i> shows helpful information and clicking on car make loads detailed data</p> <hr />
                  </header>

                <div className={`content ${isLoadingDisplay1 ? 'is-loading' : ''}`}>
                {!isLoadingDisplay1 && <div>
                  <h3>Showing data for {this.state.importType} imports</h3>
                  <table className="table table-striped table-responsive-md btn-table">
                    <thead className="indigo white-text">
                    <tr>
                        <th>Make</th>
                        <th>Sold Amount</th>
                        <th>Lorem Ipsum</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            !isLoadingDisplay1 && cars.length > 0 ? cars.map(car => {
                                const {make, sold} = car;
                                return <DisplayComponent1 key={make} make={make} sold={sold} handler={this.handler}>                                    
                                </DisplayComponent1>
                            }) : null
                        }
                    </tbody></table> <hr /></div>}

                    {!isLoadingDisplay1 && isLoadingDisplay2 && <div>
                      <h3>Please click on a make above to show detailed sale data</h3>
                      </div>
                    }

                    {!isLoadingDisplay2 && <div>

                    <h3>{this.state.selectedMake}</h3>

                    <table className="table table-bordered table-responsive-md table2">
                    <thead className="grey white-text">
                    <tr>
                        <th>Model</th>
                        <th>Sold in NJ</th>
                        <th>Sold in PA</th>
                        <th>Sold in CT</th>
                    </tr>
                    </thead>
                    <tbody>
                        {
                            !isLoadingDisplay2 && cars2.length > 0 ? cars2.map(car => {
                                const {model, NJ, PA, CT} = car;                                
                                return <DisplayComponent2 key={model} model={model} NJ={NJ} PA={PA} CT={CT}>                                    
                                </DisplayComponent2>
                            }) : null
                        }
                    </tbody></table>
                    <hr/>
                  </div>}
                    <div className="loader">
                        <div className="icon"></div>
                    </div>
                </div>
            </div>


      </div>
    );
  }
}

export default App;